#!/bin/bash

# make_page - A script to produce an HTML file

TITLE="A simple MC ModPack for 1.7.10"
TIME_STAMP="Updated on $(date -u +%T) $( date -u +%D) GMT by $USER"

function write_from_file
{
    echo "<pre>"
while read line
do
    echo "<a>$line</a>"
done <test.txt
    echo "</pre>"
}

cat <<- _EOF_
    <HTML>
    <HEAD>
        <TITLE>
        $TITLE
        </TITLE>
    </HEAD>
    <BODY>
    <H2>$TITLE</H2>
_EOF_
    write_from_file
cat <<- _EOF_
    <P>$TIME_STAMP
    </BODY>
    </HTML>
_EOF_