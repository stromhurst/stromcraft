# List of mods to be used in the latest StromCraft server
# along with links to their respective info pages AND download link

Applied Energestics: http://ae-mod.info/Downloads/

Team CoFH: http://teamcofh.com/downloads/
	CoFH core: http://curse.com/mc-mods/minecraft/cofhcore/785608
	Thermal Expansion: http://curse.com/mc-mods/minecraft/thermalexpansion/789826
	Redstone Arsenal: http://curse.com/mc-mods/minecraft/redstone-arsenal/762740
	MineFactoryReloaded: 
	
mDiyo: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2218638-tinkers-construct
	Tinkers Construct: http://minecraft.curseforge.com/mc-mods/74072-tinkers-construct/files/2201640/download
		add ons: TOolTip: https://github.com/squeek502/TiC-Tooltips/releases
		ExtraTic: http://www.curse.com/mc-mods/minecraft/extratic#t1:other-downloads
	Mechaworks: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/wip-mods/1446244-1-6-4-tinkers-mechworks
	Natura: http://www.minecraftforum.net/topic/2605688-172natura/
	
Tomahawk: http://www.minecraftmods.com/tomahawk-mod/
	download: http://www.mediafire.com/download/60yko2x22au7p5w/tomahawk-1.3.0.0-1.7.2.jar

Greg SD mod: http://www.minecraftforge.net/forum/index.php?topic=5594.0
	download: 
	
Project Red: http://projectredwiki.com/wiki/Version_archive#Downloads
	 Base, Compat, Integration, Lighting, Mechanical_beta, World
	 
Rail Craft: http://www.railcraft.info/releases
	steve carts: http://stevescarts2.wikispaces.com/Download
	
Statues: 
	AiseLib 

Artifice: http://www.curse.com/mc-mods/minecraft/223174-artifice#t1:other-downloads

Waila : http://minecraft.curseforge.com/mc-mods/73488-waila/files

Twilight Forest: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1276258-the-twilight-forest-v2-3-1-snow-queen-troll

Mekanism: http://aidancbrady.com/mekanism/download/
	mekanism main, genertors, tools
	
openMods: http://www.openmods.info:8080/
    openBlocks

Bibliocraft: http://www.bibliocraftmod.com/?page_id=50
	addons for Forestry, BiomesOPlenty, Natura

crafting Pillar: http://www.9minecraft.net/crafting-pillar-mod/

soul shards: http://www.9minecraft.net/soul-shards-reborn-mod/

Inventory Tweaks: http://inventory-tweaks.readthedocs.org/en/latest/

Hardcore Ender Expansion: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1281889-hardcore-ender-expansion-v1-6-3-120-000-dls

Extra Foods: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2213570-1-7-10-extra-food

Malisis doors: http://minecraft.curseforge.com/mc-mods/223891-malisis-doors

Magical Crops: http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/1287451-magical-crops-farm-your-resources-3-2-0-now-with