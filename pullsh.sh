#!/bin/sh
cd /home/strom/minecraft/stromDev/gitStromPack/stromcraft/
echo "Pulling from the repo"
git pull origin master
echo "Pull complete"
echo "Copying all mods and configs over to the minecraft dir"
rm -r ../../stromPack/mods/
cp ../../stromPack/config/Strom* ../../importantFiles/
rm -r ../../stromPack/config/
cp -r config/ mods/ ../../stromPack/
cp ../../importantFiles/stromPlugin* ../../stromPack/mods/
cp ../../importantFiles/Strom* ../../stromPack/config/
cp ../../importantFiles/mysql* ../../stromPack/mods/
echo "done copying, need to restart the server to take effect"
echo "copying mods and config folders from here to a working directory"
rm -r /home/strom/minecraft/stromDev/gitStromPack/workingDir/config/
rm -r /home/strom/minecraft/stromDev/gitStromPack/workingDir/mods/
cp -r mods/ config/ /home/strom/minecraft/stromDev/gitStromPack/workingDir/
echo "forge.1217-"$(git rev-list --count HEAD) > ../latestBuild.txt
echo "copied files, now running the zip and publishing script"
sh ../workingDir/zipTheseFiles.sh
echo "all scripts done"
sh /home/strom/wwwSTUFF/magtec/scripts/genModPacklink.sh > /home/strom/wwwSTUFF/magtec/htdocs/modDownloadRedir.html
sh /home/strom/minecraft/stromDev/gitStromPack/stromcraft/website/scripts/gen_mod_list.sh
mv test.txt TempModlist.txt
